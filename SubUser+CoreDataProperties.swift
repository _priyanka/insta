//
//  SubUser+CoreDataProperties.swift
//  
//
//  Created by Sierra 4 on 17/02/17.
//
//  This file was automatically generated and should not be edited.
//

import Foundation
import CoreData


extension SubUser {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<SubUser> {
        return NSFetchRequest<SubUser>(entityName: "SubUser");
    }

    @NSManaged public var subUserDP: String?
    @NSManaged public var subUserId: String?
    @NSManaged public var subUserName: String?

}
