//
//  UserData+CoreDataProperties.swift
//  
//
//  Created by Sierra 4 on 17/02/17.
//
//  This file was automatically generated and should not be edited.
//

import Foundation
import CoreData


extension UserData {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<UserData> {
        return NSFetchRequest<UserData>(entityName: "UserData");
    }

    @NSManaged public var userComment: String?
    @NSManaged public var userDP: String?
    @NSManaged public var userId: String?
    @NSManaged public var userName: String?
    @NSManaged public var userNewsFeedPicture: String?
    @NSManaged public var type: String?
    @NSManaged public var usernewsFeedvideo: String?

}
