//
//  DataInCollectionViewCell.swift
//  InstagramTaskFinl
//
//  Created by Sierra 4 on 15/02/17.
//  Copyright © 2017 Sierra 4. All rights reserved.
//

import UIKit

class DataInCollectionViewCell: UICollectionViewCell
{
    
    @IBOutlet weak var imgCollectionView: UIImageView!
    
    @IBOutlet weak var lblUserName: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
